import time
import signal
import tello

def handler(signum, frame):
    pass

#try:
#    signal.signal(signal.SIGHUP, handler)
#except AttributeError:
#    # Windows compatibility
#    pass

print('starting')

import pygame

import os
os.environ["SDL_VIDEODRIVER"] = "dummy"

# Setup Tello
mDrone = tello.Tello()
RC_VAL_MIN     = 364
RC_VAL_MID     = 1024
RC_VAL_MAX     = 1684

IDX_ROLL       = 0
IDX_PITCH      = 1
IDX_THR        = 2
IDX_YAW        = 3
###############################################################################
# global variables
###############################################################################
mKeyFlags    = 0
mOldKeyFlags = 0
mRCVal       = [1024, 1024, 1024, 1024]
mStickSpeed  = 0


def recv():
    count = 0
    while True:
        try:
            data, server = sock.recvfrom(1518)
            print(data.decode(encoding="utf-8"))
        except Exception:
            print ('\nExit . . .\n')
            break

time.sleep(2)

# Initialise the pygame library
pygame.init()
pygame.mixer.init()
pygame.display.set_mode((600, 400)) # Set dummy display

#pygame.mixer.init()
print("Pygame init done.")

flying=False

joystickAvailable=False
while pygame.joystick.get_count() == 0:
  print("ps3 controller not connected.")
  time.sleep(5)
  exit()

# Connect to the first JoyStick
j = pygame.joystick.Joystick(0)
j.init()

print 'Initialized Joystick : %s' % j.get_name()
print "Number of buttons: %d" % j.get_numbuttons()

###############################################################################
# main
###############################################################################
print 'Tello PS3 Controller                  '
print '+------------------------------------+'
print '|   ^C(quit)                         |'
print '+------------------------------------+'
print '|                                    |'
print '| Left Joystick      Right Joystick  |'
print '|      up               forward      |'
print '|  ccw     cw        left    right   |'
print '|     down              backwards    |'
print '|                                    |'
print '|         start(takeoff/land)        |'
print '|        select(beginner/expert)     |'
print '|                                    |'
print '+------------------------------------+'


done=False
while done == False:
  try:
    events = pygame.event.get()
    for event in events:
      #print 'type: %d' % event.type
  
      if event.type == pygame.JOYAXISMOTION:
        if event.axis == 0: # Left/right on left stick
          mRCVal[IDX_YAW] = RC_VAL_MID
          value=(event.value*255)
          if value < 20 and value > -20:
            value = 0
          if value != 0:
            print 'axis 0 left/right %d' % (value)
            if value > 0:
              print 'cw'
              mRCVal[IDX_YAW] = RC_VAL_MAX
            else:
              print 'ccw'
              mRCVal[IDX_YAW] = RC_VAL_MIN
        if event.axis == 1: # Up/Down on left stick
          mRCVal[IDX_THR] = RC_VAL_MID
          value=-(event.value*255)
          if value < 20 and value > -20:
            value = 0
          if value != 0:
            if value > 0:
              print 'up'
              mRCVal[IDX_THR] = RC_VAL_MAX
            else:
              print 'down'
              mRCVal[IDX_THR] = RC_VAL_MIN
        if event.axis == 2: # 
          value=(event.value*255)
          if value < 5 and value > -5:
            value = 0
          if value != 0:
            print 'axis 2 %d' % (value)
        if event.axis == 3: #  Left/Right on right stick
          mRCVal[IDX_ROLL] = RC_VAL_MID
          #value=(event.value*255)
          value=(event.value*660)
          if value < 20 and value > -20:
            value = 0
          if value != 0:
            if value > 0:
              print 'left'
              mRCVal[IDX_ROLL] = RC_VAL_MAX
            else:
              print 'right'
              mRCVal[IDX_ROLL] = RC_VAL_MIN
        if event.axis == 4: #  Up/Down on right stick
          mRCVal[IDX_PITCH] = RC_VAL_MID
          value=-(event.value*255)
          if value < 20 and value > -20:
            value = 0
          if value != 0:
            print 'axis 4 %d' % (value)
          if value != 0:
            if value > 0:
              print 'Forward'
              mRCVal[IDX_PITCH] = RC_VAL_MAX
            else:
              print 'Backwards'
              mRCVal[IDX_PITCH] = RC_VAL_MIN
        if event.axis == 12: # Left trigger bottom button
          value=-(event.value*127)
          value = value + 127
          value = 255 - value
          value = -(value)
          if value < 5 and value > -5:
            value = 0
          print 'axis 12 trigger %d' % (value)
        if event.axis == 14: # Left trigger top button Reverse
          value=-(event.value*127)
          value = value + 127
          value = 255 - value
          if value < 5 and value > -5:
            value = 0
          print 'axis 14 trigger %d' % (value)
        if event.axis == 15: # Right trigger top button Reverse
          value=-(event.value*127)
          value = value + 127
          value = 255 - value
          value = -(value)
          if value < 5 and value > -5:
            value = 0
          print 'axis 15 trigger %d' % (value)
        if event.axis == 13: # Right trigger bottom button
          value=-(event.value*127)
          value = value + 127
          value = 255 - value
          if value < 5 and value > -5:
            value = 0
          print 'axis 13 trigger %d' % (value)
  
      if event.type == pygame.JOYBUTTONDOWN:
        print 'Button press %d' % (event.button)
        if event.button == 9: # Start button
          # Do a takeoff or land
          if flying == False:
            flying = True
            print 'Takeoff'
            mDrone.takeOff()
          else:
            flying = False
            print 'Land'
            mDrone.land()
        if event.button == 8: # Set speed
          if mStickSpeed ==  1:
            print 'Set mode to slow'
            mStickSpeed = 0
          else:
            print 'Set mode to fast'
            mStickSpeed = 1

        # Not implemented yet.
        if event.button == 0:
          print 'Front flip'
        if event.button == 1:
          print 'Right flip'
        if event.button == 2:
          print 'Back flip'
        if event.button == 3:
          print 'Left flip'
  
      mDrone.setStickData(mStickSpeed, mRCVal[IDX_ROLL], mRCVal[IDX_PITCH], mRCVal[IDX_THR], mRCVal[IDX_YAW])
  except (KeyboardInterrupt, SystemExit):
    print '\nkeyboardinterrupt caught'
    done=True

mDrone.stop()